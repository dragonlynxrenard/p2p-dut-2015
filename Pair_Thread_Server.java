import java.io.IOException;



public class Pair_Thread_Server implements Runnable {
	
	private Pair pair;

	/*Constructeur de la classe*/
	public Pair_Thread_Server(Pair pair) {
		this.pair = pair;
	}

	//Implémentation de la méthode run (interface Runnable)
	@Override
	public void run() {
		System.out.println("The system is running !");
		while (true) {
			try {
				String str = this.pair.getEntree().readLine();
				System.out.println("You just received : " + str);
				//Si l'on reçoit une demande pour joindre le réseau
				switch(str.substring(0,3)){
					case "?cc": 
						this.pair.recieve_reach(str.substring(3));
						break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NullPointerException npe) {
				System.out.println("A peer just quit the network");
				break;
			}
			
		}
	}

}
