import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Scanner;


public class Pair {
	
	private ServerSocket server;
	private BufferedReader entree;
	private PrintWriter sortie;
	private HashMap<Integer, String> route;
	private int predecesseur;
	private int successeur;
	private String ip;
	
	
	/*Constructeur*/
	public Pair() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("join or create ?");
		boolean continu = false;
		do {
			try {
				String str = sc.nextLine();
				
				this.route = new HashMap<Integer, String>();
				
				if (str.equals("join")) {
					this.join();
					continu = false;
				}
				else if (str.equals("create")) {
					this.create();
					continu = false;
				} 
				else {
					continu = true;
				}
			} catch (NoSuchElementException nee) {
				System.out.println("Fin prématurée du programme.");
			}
		} while(continu);
	}
	
	/*Méthode pour rejoindre un réseau existant en demandant l'ip d'un pair connu*/
	public void join() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("ip ?");
		
		try {
		
			/*Connexion vers le pair connu*/
			String ip = sc.nextLine();
			this.server = new ServerSocket(4242);
			Socket socket = new Socket(ip, 4242);
			
			this.ip = InetAddress.getLocalHost().toString().split("/")[1];
			
			InputStream fluxEntree = socket.getInputStream();
			OutputStream fluxSortie = socket.getOutputStream();
			
			this.entree = new BufferedReader(new InputStreamReader(fluxEntree));
			this.sortie = new PrintWriter(fluxSortie, true);
			
			/*Attente de la reception du message permettant de connaitre
			son successeur et son predecesseur*/
			while(!this.reach_network(ip));
			
			System.out.println("Réunion avec le réseau réussie.");
			System.out.println(this.toString()); 
			
			/*Démarrage du Thread pour recevoir de nouveaux messages*/
			while (true) {
				
				Socket sock = this.server.accept();
				
				fluxEntree = sock.getInputStream();
				fluxSortie = sock.getOutputStream();
				
				this.entree = new BufferedReader(new InputStreamReader(fluxEntree));
				this.sortie = new PrintWriter(fluxSortie, true);
				
				Pair_Thread_Server pts = new Pair_Thread_Server(this);
				Thread tpts = new Thread(pts);
				tpts.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchElementException nee) {
			System.out.println("Fin prématurée du programme");
		}
		
	}
	
	/*Méthode pour créer un nouveu réseau*/
	public void create() {
		try {			
			this.server = new ServerSocket(4242);
			this.ip = InetAddress.getLocalHost().toString().split("/")[1];
			this.join_network(this.ip, this.ip);
			
			System.out.println("Création du réseau réussie.");
			System.out.println(this.toString());
			
			/*Démarrage du Thread pour recevoir de nouveaux messages*/
			while (true) {
				
				Socket sock = this.server.accept();
				
				InputStream fluxEntree = sock.getInputStream();
				OutputStream fluxSortie = sock.getOutputStream();
				
				this.entree = new BufferedReader(new InputStreamReader(fluxEntree));
				this.sortie = new PrintWriter(fluxSortie, true);
				
				Pair_Thread_Server pts = new Pair_Thread_Server(this);
				Thread tpts = new Thread(pts);
				tpts.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/*Réception d'un message de demande pour venir dans le réseau*/
	public void recieve_reach(String ip){
		try {
			System.out.println("Received request from " + ip.hashCode() + " (" + ip + ").");
			//Cas s'il n'y a qu'un seul pair
			if (this.hash() == this.successeur) {
				this.sortie.println("!ps" + this.ip + "-" + this.route.get(this.successeur));
				//this.sortie.close();
				this.join_network(ip, ip);
			} 
			//Si l'on est le futur prédecesseur
			else if ((this.successeur > ip.hashCode() && this.hash() < ip.hashCode() && this.hash() <= this.successeur)
					|| (this.hash() < ip.hashCode() && this.hash() > this.successeur)
					|| (this.hash() > ip.hashCode() && this.successeur > this.hash())){
				this.sortie.println("!ps" + this.ip + "-" + this.route.get(this.successeur));
				//this.sortie.close();
				this.successeur = ip.hashCode();
				this.route.put(this.successeur, ip);
			} 
			//Sinon on renvoie le message à son successeur
			else {
				Socket sock_succ = new Socket(this.route.get(this.successeur), 4242);
				OutputStream fluxSortie = sock_succ.getOutputStream();
				PrintWriter sortie_succ = new PrintWriter(fluxSortie);
				sortie_succ.println("?cc" + ip);
				sock_succ.close();
			}
			System.out.println(this.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	//Méthode pour rejoindre le réseau
	private boolean reach_network(String ip){ 
		try {
			System.out.println("Trying to reach the network of " + ip.hashCode() + " (" + ip + ")...");
			this.sortie.println("?cc" + this.ip);
			System.out.println("?cc" + this.ip);
			
			String reponse = this.entree.readLine();
			System.out.println ("Received : " + reponse);
			if (reponse.startsWith("!ps"))
			{
				String[] ip_p_s = reponse.substring(3).split("-");
				this.join_network(ip_p_s[0], ip_p_s[1]);
				System.out.println("Connected. Communication with the successeur.");
			} else {
				System.out.println("Received unexpected message : " + reponse);
				return false;
			}
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return false;
	}
	
	//Méthode pour changer ses informations (predecesseur, successeur, table de routage)
	public void join_network(String ip_p, String ip_s){
		this.change_succ(ip_s);
		this.change_pred(ip_p);
		System.out.println(this.toString());
	}
	
	//Méthode pour quitter le réseau (Non utilisée)
	private void quit() {
		System.out.println("Trying to quit the network, sending information to the successeur and the predecesseur...");
		try {
			//Envoi au prédecesseur des informations sur son successeur
			Socket sock = new Socket(this.route.get(this.predecesseur), 4242);
			OutputStream fluxSortie = sock.getOutputStream();
			PrintWriter sortie = new PrintWriter(fluxSortie);
			sortie.println("!qp" + this.route.get(this.successeur)); //Envoi au pr�decesseur
			sock.close();
			
			//Envoi au successeur des informations sur son pr�decesseur
			sock = new Socket(this.route.get(this.successeur), 4242);
			fluxSortie = sock.getOutputStream();
			sortie = new PrintWriter(fluxSortie);
			sortie.println("!qs" + this.route.get(this.predecesseur)); //Envoi au successeur
			sock.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		System.out.println("Exit succeed. Good bye !");
	}
	
	//Méthode pour recherche quelqu'un dans le réseau (non utilisée)
	private void look_for_someone(int hash){
		try {
			Socket sock = new Socket(this.route.get(this.predecesseur), 4242);
			OutputStream fluxSortie = sock.getOutputStream();
			PrintWriter sortie = new PrintWriter(fluxSortie);
			sortie.println("?lf" + this.ip + "-" + hash);
			sock.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	//Méthode pour dire que l'on est la personne recherchée (non utilisée)
	private void its_me(String ip){
		try {
			Socket sock = new Socket(ip, 4242);
			OutputStream fluxSortie = sock.getOutputStream();
			PrintWriter sortie = new PrintWriter(fluxSortie);
			sortie.println("!im" + this.ip);
			sock.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	//Méthode pour changer son successeur et modifier la table de routage
	private void change_succ(String ip) {
		this.successeur = ip.hashCode();
		this.route.put(this.successeur, ip);
	}
	
	//Méthode pour changer son prédecesseur et modifier la table de routage
	private void change_pred(String ip) {
		this.predecesseur = ip.hashCode();
		this.route.put(this.predecesseur, ip);
	}
	
	//Méthode pour obtenir l'identifiant du pair, c'est-à-dire le hash de son adresse ip
	public int hash() {
		return this.ip.hashCode();
	}
	
	
	/*
	Getter et setter
	*/
	public ServerSocket getServer() {
		return server;
	}

	public void setServer(ServerSocket server) {
		this.server = server;
	}

	public BufferedReader getEntree() {
		return entree;
	}

	public void setEntree(BufferedReader entree) {
		this.entree = entree;
	}

	public PrintWriter getSortie() {
		return sortie;
	}

	public void setSortie(PrintWriter sortie) {
		this.sortie = sortie;
	}

	public HashMap<Integer, String> getRoute() {
		return route;
	}

	public void setRoute(HashMap<Integer, String> route) {
		this.route = route;
	}

	public int getPredecesseur() {
		return predecesseur;
	}

	public void setPredecesseur(int predecesseur) {
		this.predecesseur = predecesseur;
	}

	public int getSuccesseur() {
		return successeur;
	}

	public void setSuccesseur(int successeur) {
		this.successeur = successeur;
	}
	
	public String toString(){
		String msg = "Information about that peer :\n";
		msg += "My IP           : " + this.ip + "\n";
		msg += "My root table   : " + this.route.toString() + "\n";
		msg += "My successeur   : " + this.successeur + "\n";
		msg += "My predecesseur : " + this.predecesseur;
		return msg;
	}

	public static void main(String[] args) {
		new Pair();
	}
}
